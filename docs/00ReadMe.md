# Making GC zeta scaled abundance sets
v1.0.12

## Files

1. Master file: `GC_Masterv2.6.4.txt` Contains the GC2017
abundances, and the corresponding Iron scale zeta abundnace
ratios Xi(El), and Fe break points. `GC_Master_Xi0.txt` is the
equivalent file for the simple scaling options with Xi = 0.

2. `gczeta.rb`  script that reads the Master file and then
reads an input list of zetas to make .abn files.
`gcscale.rb`, unifomr scaling except for Helium and Nitrogen
`gcsimplescale.rb` simple uniform M/H scaling for all but Helium.

3. `zeta_XXX.txt`  input files.
Input file format,

    * line 1: `Zetas: Linear` or `Zetas: Log` indicates whether following numbers are linear or log... :)
    * Line 2: `Base: El` where El can be C, O....Zn  ie any element symbol except H, He, i, Be, B, or N
    * Lines 3  to  end: One number per line of zeta values.

See input files , ie `zeta_input.txt`

## Output .abns

Which are text files, edit the ruby scripts if you prefer other suffixes

The header contain fairly complete information on the file origins and contents.  Ready to run in MAPPINGS V  (or IV)

## Useage:

Make GC2017 files equiv to solar oxygen scales

    > ./gczeta.rb zeta_input.txt

make files to match HII grid abundances in Fe or O:

    > ./gczeta.rb zeta_AGGCFe.txt
    > ./gczeta.rb zeta_AGGCO.txt

Enjoy!
